﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowAssign.Pages
{
    public class ProductDetailPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        By addToCart_btn =By.XPath( "//input[@title='Add to Shopping Cart']");
        By price = By.XPath("//div[@id='usedBuySection']/div[1]/div/span[2]");
       



        public ProductDetailPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait=new WebDriverWait(driver,TimeSpan.FromSeconds(40));
        }

        public CartPage clickOnAddToCartBtn()
        {
            wait.Until(ExpectedConditions.ElementExists(addToCart_btn)).Click();
            return new CartPage(driver);
        }

        public string getPrice()
        {
            string actual_price=wait.Until(ExpectedConditions.ElementExists(price)).Text;
            return actual_price;
        }
    }
}
