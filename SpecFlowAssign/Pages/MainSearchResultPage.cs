﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowAssign.Pages
{
    public class MainSearchResultPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        By productName = By.XPath("//a/span[contains(text(),'N450 WiFi Router')]");


        public MainSearchResultPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait=new WebDriverWait(driver, TimeSpan.FromSeconds(40));
        }

        

        public ProductDetailPage clickOnProduct()
        {
            wait.Until(ExpectedConditions.ElementExists(productName)).Click();
            return new ProductDetailPage(driver);
        }

        public string getTitle()
        {
            return driver.Title;
        }
    }
}
