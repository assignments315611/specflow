﻿using OpenQA.Selenium;
using OpenQA.Selenium.DevTools.V120.Target;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowAssign.Pages
{
    public class MainPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
            
        }


        By location = By.XPath("//div[@id='nav-global-location-slot']//a");
        By zipcode = By.XPath("//*[@id='GLUXZipUpdateInput']");
        By location_btn = By.XPath("//*[@id='GLUXZipUpdate']/span/input");
        By searchTextBox = By.XPath("//input[@id='twotabsearchtextbox']");
        
            
        public void setLocation()
        {
            wait.Until(ExpectedConditions.ElementExists(location)).Click();
            Thread.Sleep(3000);
            wait.Until(ExpectedConditions.ElementExists(zipcode)).SendKeys("94020");
            Thread.Sleep(3000);

            wait.Until(ExpectedConditions.ElementExists(location_btn)).Click();
            
        }


        public MainSearchResultPage searchText(string text)
        {
            wait.Until(ExpectedConditions.ElementExists(searchTextBox)).SendKeys(text);
            wait.Until(ExpectedConditions.ElementExists(searchTextBox)).SendKeys(Keys.Enter);
            return new MainSearchResultPage(driver);

        }


    }
}
