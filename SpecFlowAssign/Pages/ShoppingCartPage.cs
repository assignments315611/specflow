﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowAssign.Pages
{
    public class ShoppingCartPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        By price = By.XPath("//div[@data-name='Subtotals']/span/span");
        By product_name = By.XPath(("//div[@class='sc-item-content-group']//a/span[1]/span/span[2]"));

        public ShoppingCartPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

        }

        public string getProductName()
        {
            string productName = wait.Until(ExpectedConditions.ElementExists(product_name)).Text;
            Console.WriteLine(productName); 
            return productName;
        }

        public string CartTotalPrice() 
        {
            string cartPrice = wait.Until(ExpectedConditions.ElementExists(price)).Text;
            Console.WriteLine(cartPrice);
            return cartPrice;
        }
    }
}
