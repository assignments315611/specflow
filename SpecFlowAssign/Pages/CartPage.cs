﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowAssign.Pages
{
    public class CartPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        By success_message = By.XPath("//div[@id='NATC_SMART_WAGON_CONF_MSG_SUCCESS']/h1");
        By cart_price = By.XPath(("//div[@id='sw-subtotal']/span[2]/span/span[1]"));
        By basket_icon = By.XPath("//div[@id=\"nav-cart-text-container\"]");
        public CartPage(IWebDriver driver)
        {
            this.driver = driver;
            this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

        }

        public string CartUpdated()
        {
            string success = wait.Until(ExpectedConditions.ElementExists(success_message)).Text;
            Console.WriteLine(success); 
            return success;
        }

        public string CartTotalPrice() 
        {
            string cartPrice = wait.Until(ExpectedConditions.ElementExists(cart_price)).Text;
            Console.WriteLine(cartPrice);
            return cartPrice;
        }

        public ShoppingCartPage goToShopppingCart()
        {
            wait.Until(ExpectedConditions.ElementExists(basket_icon)).Click();
            return new ShoppingCartPage(driver);
        }
    }
}
