﻿Feature: AmazonSearchProduct

A short summary of the feature

@tag1
Scenario Outline: Search for a router and verify cart price
    Given I navigate to Amazon homepage
    When I search a product "<product>"
    And I select the first product from the search results
    And I add it to the cart
    Then the product price should match the cart price
    And the product in the cart matches with the actual "<product>"

    Examples:
    | product |
    | TP-Link N450 WiFi Router - Wireless Internet Router for Home (TL-WR940N) |
