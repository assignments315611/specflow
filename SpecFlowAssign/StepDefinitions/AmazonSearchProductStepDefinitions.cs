using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlowAssign.Pages;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowAssign.StepDefinitions
{
    [Binding]
    public class AmazonSearchProductStepDefinitions
    {
        private IWebDriver driver;

        MainPage mainPage;
        MainSearchResultPage mainSearchResultPage;
        ProductDetailPage productDetailPage;
        CartPage cartPage;
        ShoppingCartPage shoppingCartPage;

        private string product_price;
        private string cart_price;
        private string product_name;

        public AmazonSearchProductStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
            this.mainPage=new MainPage(driver);
            this.mainSearchResultPage = new MainSearchResultPage(driver);
            this.productDetailPage = new ProductDetailPage(driver);
            this.cartPage = new CartPage(driver);
            this.shoppingCartPage = new ShoppingCartPage(driver);   

        }

        [Given(@"I navigate to Amazon homepage")]
        public void GivenINavigateToAmazonHomepage()
        {

            driver.Url = "https://amazon.com/";
            Thread.Sleep(15000);

        }

        [When(@"I search a product ""([^""]*)""")]
        public void WhenISearchAProduct(string p0)
        {

            mainPage.setLocation();
            mainPage.searchText(p0);
            Thread.Sleep(3000);

        }


        [When(@"I select the first product from the search results")]
        public void WhenISelectTheFirstProductFromTheSearchResults()
        {
            mainSearchResultPage.clickOnProduct();
            Assert.IsTrue(mainSearchResultPage.getTitle() != null && mainSearchResultPage.getTitle().Contains("TP-Link N450"));
            product_price = productDetailPage.getPrice();
            Console.WriteLine(product_price);
        }

        [When(@"I add it to the cart")]
        public void WhenIAddItToTheCart()
        {
            productDetailPage.clickOnAddToCartBtn();
        }

        [Then(@"the product price should match the cart price")]
        public void ThenTheProductPriceShouldMatchTheCartPrice()
        {
            cartPage.goToShopppingCart();
            
            cart_price = shoppingCartPage.CartTotalPrice();
            Assert.AreEqual(cart_price, product_price);

            Console.WriteLine(cart_price);
        }

        [Then(@"the product in the cart matches with the actual ""([^""]*)""")]
        public void ThenTheProductInTheCartMatchesWithTheActual(string p0)
        {
            product_name=shoppingCartPage.getProductName();
            Assert.AreEqual(product_name, p0);
        }

    }
}
