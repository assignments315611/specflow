﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools.V120.Network;
using TechTalk.SpecFlow;

namespace SpecFlowAssign.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private readonly IObjectContainer _container;

        public Hooks(IObjectContainer container) 
        {
            _container = container;
        }

        [BeforeScenario("@AmazonAddToCart")]
        public void BeforeScenarioWithTag()
        {
            Console.WriteLine("Running inside tag hooks");
            
        }

        [BeforeScenario(Order = 1)]
        public void FirstBeforeScenario()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            _container.RegisterInstanceAs<IWebDriver>(driver);
        }

        [AfterScenario]
        public void AfterScenario()
        {
           var driver= _container.Resolve<IWebDriver>();

            if (driver != null)
            {
                driver.Close();
            }
        }
    }
}